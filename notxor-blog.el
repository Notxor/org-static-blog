;;; notxor-blog.el --- Mi generador de blog basado en org-static-blog.

;; Author: Bastian Bechtold
;; Contrib: Shmavon Gazanchyan, Rafał -rsm- Marek, neeasade,
;; Michael Cardell Widerkrantz, Matthew Bauer, Winny, Yauhen Makei,
;; luhuaei, zngguvnf, Qiantan Hong, Jonas Bernoulli, Théo Jacquin,
;; K. Scarlet, zsxh
;; URL: https://github.com/bastibe/org-static-blog
;; Version: 1.4.0
;; Package-Requires: ((emacs "24.3"))

;;; Commentary:

;; Static blog generators are a dime a dozen.  This is one more, which
;; focuses on being simple.  All files are simple org-mode files in a
;; directory.  The only requirement is that every org file must have a
;; #+TITLE and a #+DATE, and optionally, #+FILETAGS.

;; Set up your blog by customizing notxor-blog's parameters, then
;; call M-x notxor-blog-publish to render the whole blog or M-x
;; notxor-blog-publish-file filename.org to render only only the
;; file filename.org.

;; Above all, I tried to make notxor-blog as simple as possible.
;; There are no magic tricks, and all of the source code is meant to
;; be easy to read, understand and modify.

;; If you have questions, if you find bugs, or if you would like to
;; contribute something to notxor-blog, please open an issue or
;; pull request on Github.

;; Finally, I would like to remind you that I am developing this
;; project for free, and in my spare time.  While I try to be as
;; accomodating as possible, I can not guarantee a timely response to
;; issues.  Publishing Open Source Software on Github does not imply
;; an obligation to *fix your problem right now*.  Please be civil.

;; Modificado para servir a mi blog personal «Notxor tiene un blog» en
;; https://notxor.nueva-actitud.org

;;; Code:

;;(require 'org)
(require 'ox-html)

(defgroup notxor-blog nil
  "Settings for a static blog generator using 'org-mode'."
  :version "1.4.2"
  :group 'applications)

(defcustom notxor-blog-publish-url "https://notxor.nueva-actitud.org/"
  "URL of the blog."
  :type '(string)
  :safe t)

(defcustom notxor-blog-publish-title "Notxor tiene un blog"
  "Title of the blog."
  :type '(string)
  :safe t)

(defcustom notxor-blog-publish-directory "~/public_html/"
  "Directory where published HTML files are stored."
  :type '(directory))

(defcustom notxor-blog-posts-directory "~/blog/articulos/"
  "Directory where published ORG files are stored.
When publishing, posts are rendered as HTML, and included in the
index, archive, tags, and RSS feed."
  :type '(directory))

(defcustom notxor-blog-drafts-directory "~/blog/borradores/"
  "Directory where unpublished ORG files are stored.
When publishing, draft are rendered as HTML, but not included in
the index, archive, tags, or RSS feed."
  :type '(directory))

(defcustom notxor-blog-index-file "index.html"
  "File name of the blog landing page.
The index page contains the most recent
`notxor-blog-index-length` full-text posts."
  :type '(string)
  :safe t)

(defcustom notxor-blog-index-length 10
  "Number of articles to include on index page."
  :type '(integer)
  :safe t)

(defcustom notxor-blog-archive-file "archivo.html"
  "File name of the list of all blog posts.
The archive page lists all posts as headlines."
  :type '(string)
  :safe t)

(defcustom notxor-blog-tags-file "etiquetas.html"
  "File name of the list of all blog posts by tag.
The tags page lists all posts as headlines."
  :type '(string)
  :safe t)

(defcustom notxor-blog-enable-tags t
  "Show tags below posts, and generate tag pages."
  :group 'notxor-blog
  :type '(boolean)
  :safe t)

(defcustom notxor-blog-enable-rss-by-tag t
  "Generate rss files by tag."
  :group 'notxor-blog
  :type '(boolean)
  :safe t)

(defcustom notxor-blog-enable-deprecation-warning t
  "Show deprecation warnings."
  :type '(boolean))

(defcustom notxor-blog-rss-file "rss.xml"
  "File name of the RSS feed."
  :type '(string)
  :safe t)

(defcustom notxor-blog-rss-extra ""
  "Extra information for the RSS feed header.
This information is placed right before the sequence of posts.
You can add an icon for the feed, or advertise that you built
your blog with Emacs, `org-mode` and notxor-blog."
  :type '(string)
  :safe t)

(defcustom notxor-blog-page-header
  "<meta name=\"author\" content=\"Notxor\">
  <meta name=\"referrer\" content=\"no-referrer\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <link href=\"/medios/css/estilos.css\" rel=\"stylesheet\" type=\"text/css\" />
  <link rel=\"icon\" href=\"/medios/img/favicon.png\">"
  "HTML to put in the <head> of each page."
  :type '(string)
  :safe t)

(defcustom notxor-blog-page-preamble
  "<div>
      <header class=\"masthead\">
        <h1 class=\"masthead-title\"><a href=\"/\">Notxor tiene un blog</a></h1>
        <h2 class=\"masthead-subtitle\">Defenestrando la vida</h2>
	<img class=\"avatar\" src=\"/medios/img/abatar.png\" width=\"100px\"></img>
        <ul>
          <li><a href=\"/\">Blog</a></li>
          <li><a href=\"/tags/esperanto\">Esperanto</a></li>
          <li><a href=\"http://asociacionpica.org/radio\" target=\"_blank\">Radio</a></li>
          <li><a href=\"http://blog-antiguo.nueva-actitud.org\" target=\"_blank\">Blog antiguo</a></li>
          <li><a href=\"/etiquetas.html\">Etiquetas</a></li>
          <li><a href=\"/archivo.html\">Archivo</a></li>
          <li><a href=\"/about/\">Acerca de...</a></li>
          <li><a href=\"/rss.xml\">RSS</a></li>
        </ul>
      </header>
    </div>"
  "HTML to put before the content of each page."
  :type '(string)
  :safe t)

(defcustom notxor-blog-page-postamble
   "<div>
    <script src=\"/medios/js/jquery-latest.min.js\"></script>
    <script src=\"https://polyfill.io/v3/polyfill.min.js?features=es6\"></script>
    <script id=\"MathJax-script\" async src=\"https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js\"></script>
    <script src=\"/medios/js/main.js\"></script>
    <div class=\"footer\">
        <div class=\"redes\">
            <p>
                <a rel=\"me\" href=\"https://tuiter.rocks/@Notxor\" target=\"_blank\"><img src=\"/medios/img/mastodon-icono.png\" width=\"24px\" float=\"left\" /></a>
                <a rel=\"me\" href=\"https://masto.es/@Notxor\" target=\"_blank\"><img src=\"/medios/img/mastodon-icono.png\" width=\"24px\" float=\"left\" /></a>
                <a href=\"https://diasp.org/people/15172490a88f01339a3f782bcb452bd5\" target=\"_blank\"><img src=\"/medios/img/diaspora-icono.png\" width=\"24px\" float=\"left\" /></a>
            </p>
        </div>
        <p>Generado con <code>notxor-blog</code> en <i>Emacs</i></p>
        <p>
            <img style=\"display: inline-block;\" src=\"/medios/img/cc-by-nc-sa.png\" align=\"middle\" />
               <br />2012 - <span id=\"footerYear\"></span> <a href=\"mailto:notxor@nueva-actitud.org\">Notxor</a>
            &nbsp;&nbsp;-&nbsp;&nbsp;
            Powered by <a href=\"https://codeberg.org/Notxor/org-static-blog\" target=\"_blank\">notxor-blog</a>
            <script type=\"text/javascript\">document.getElementById(\"footerYear\").innerHTML = (new Date()).getFullYear();</script>
        </p>
        <table>
            <tr>
                <td>
                    <b>Donar por Paypal</b>
                </td>
                <td>
                    <b>Donar por Liberapay</b>
                </td>
            </tr>
            <tr>
                <td>
                    <form action=\"https://www.paypal.com/donate\" method=\"post\" target=\"_top\">
                    <input type=\"hidden\" name=\"hosted_button_id\" value=\"GV3VUVFLDAC3J\" />
                    <input type=\"image\" src=\"https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donate_LG.gif\" border=\"0\" name=\"submit\" title=\"PayPal - The safer, easier way to pay online!\" alt=\"Botón Donar con PayPal\" />
                    <img alt=\"\" border=\"0\" src=\"https://www.paypal.com/es_ES/i/scr/pixel.gif\" width=\"1\" height=\"1\" />
                    </form>
                </td>
                <td>
                    <!-- Inserción de botón liberapay -->
                    <script src=\"https://liberapay.com/Notxor/widgets/button.js\"></script>
                    <noscript>
                        <a href=\"https://liberapay.com/Notxor/donate\">
                            <img alt=\"Donate using Liberapay\" src=\"https://liberapay.com/assets/widgets/donate.svg\"></a>
                    </noscript>
                </td>
            </tr>
        </table>
    </div>
</div>"
  "HTML to put after the content of each page."
  :type '(string)
  :safe t)

(defcustom notxor-blog-post-comments
  "<section>
        <h1>Comentarios</h1>
        <blockquote>
            <p>Debido a algunos ataques mailintencionados a través de la
                herramienta de comentarios, he decidido no proporcionar
                dicha opción en el <i>Blog</i>. Si alguien quiere comentar
                algo, me puede encontrar en <a rel=\"me\" href=\"https://masto.rocks/web/@Notxor\" target=\"_blank\"><i> esta cuenta de Mastodon</i></a>,
                también en <a rel=\"me\" href=\"https://masto.es/@Notxor\" target=\"_blank\"><i>esta otra cuenta de Mastodon</i></a>
                y en <a href=\"https://diasp.org/people/15172490a88f01339a3f782bcb452bd5\" target=\"_blank\"><i>Diaspora</i></a>
                con el <i>nick</i> de <b>Notxor</b>.</p>
            <p>Si usas habitualmente XMPP (si no, te recomiendo que lo hagas), puedes encontrar también un pequeño grupo en el siguiente enlace:
             <a href=\"xmpp:notxor-tiene-un-blog@salas.suchat.org?join\" target=\"_blank\">notxor-tiene-un-blog@salas.suchat.org</a></p>
            <p>Disculpen las molestias.</p>
            <p>Escrito enteramente por mi <i>Estulticia Natural</i> sin intervención de ninguna IA.</p>
        </blockquote>
    </section>"
  "HTML code for comments to put after each blog post."
  :type '(string)
  :safe t)

(defcustom notxor-blog-langcode "es"
  "Language code for the blog content."
  :type '(string)
  :safe t)

(defcustom notxor-blog-use-preview t
  "Use preview versions of posts on multipost pages.

See also `notxor-blog-preview-ellipsis' and
`notxor-blog-preview-link-p'."
  :type '(boolean)
  :safe t)

(defcustom notxor-blog-preview-convert-titles t
  "When preview is enabled, convert <h1> to <h2> for the previews."
  :type '(boolean)
  :safe t)

(defcustom notxor-blog-preview-ellipsis "(ver más...)"
  "The HTML appended to the preview if some part of the post is hidden."
  :type '(string)
  :safe t)

(defcustom notxor-blog-preview-link-p t
  "Whether to make the preview ellipsis a link to the article's page."
  :type '(boolean)
  :safe t)

;; localization support
(defconst notxor-blog-texts
  '((other-posts
     ("en" . "Other posts")
     ("eo" . "Aliaj artikoloj")
     ("pl" . "Pozostałe wpisy")
     ("ru" . "Другие публикации")
     ("by" . "Іншыя публікацыі")
     ("it" . "Altri articoli")
     ("es" . "Otros artículos")
     ("fr" . "Autres articles"))
    (date-format
     ("en" . "%d %b %Y")
     ("eo" . "%Y-%m-%d")
     ("pl" . "%Y-%m-%d")
     ("ru" . "%d.%m.%Y")
     ("by" . "%d.%m.%Y")
     ("it" . "%d/%m/%Y")
     ("es" . "%Y-%m-%d")
     ("fr" . "%d-%m-%Y"))
    (tags
     ("en" . "Tags")
     ("eo" . "Etikedoj")
     ("pl" . "Tagi")
     ("ru" . "Ярлыки")
     ("by" . "Ярлыкі")
     ("it" . "Categorie")
     ("es" . "Categoría")
     ("fr" . "Tags"))
    (archive
     ("en" . "Archive")
     ("eo" . "Arĥivaro")
     ("pl" . "Archiwum")
     ("ru" . "Архив")
     ("by" . "Архіў")
     ("it" . "Archivio")
     ("es" . "Archivo")
     ("fr" . "Archive"))
    (posts-tagged
     ("en" . "Posts tagged")
     ("eo" . "Etikedigitaj artikoloj")
     ("pl" . "Wpisy z tagiem")
     ("ru" . "Публикации с ярлыками")
     ("by" . "Публікацыі")
     ("it" . "Articoli nella categoria")
     ("es" . "Artículos de la categoría")
     ("fr" . "Articles tagués"))
    (no-prev-post
     ("en" . "There is no previous post")
     ("eo" . "Ne estas antaŭa artikolo")
     ("pl" . "Poprzedni wpis nie istnieje")
     ("ru" . "Нет предыдущей публикации")
     ("by" . "Няма папярэдняй публікацыі")
     ("it" . "Non c'è nessun articolo precedente")
     ("es" . "No existe un artículo precedente")
     ("fr" . "Il n'y a pas d'article précédent"))
    (no-next-post
     ("en" . "There is no next post")
     ("eo" . "Ne estas sekvanta artikolo")
     ("pl" . "Następny wpis nie istnieje")
     ("ru" . "Нет следующей публикации")
     ("by" . "Няма наступнай публікацыі")
     ("it" . "Non c'è nessun articolo successivo")
     ("es" . "No hay artículo siguiente")
     ("fr" . "Il n'y a pas d'article suivants"))
    (title
     ("en" . "Title: ")
     ("eo" . "Titolo: ")
     ("pl" . "Tytuł: ")
     ("ru" . "Заголовок: ")
     ("by" . "Загаловак: ")
     ("it" . "Titolo: ")
     ("es" . "Título: ")
     ("fr" . "Titre : "))
    (author
     ("en" . "Author: ")
     ("eo" . "Verkisto: ")
     ("es" . "Autor: "))
    (filename
     ("en" . "Filename: ")
     ("eo" . "Arĥiva nomo: ")
     ("pl" . "Nazwa pliku: ")
     ("ru" . "Имя файла: ")
     ("by" . "Імя файла: ")
     ("it" . "Nome del file: ")
     ("es" . "Nombre del archivo: ")
     ("fr" . "Nom du fichier :"))))

(defun notxor-blog-gettext (text-id)
  "Return localized TEXT-ID.
Depends on `notxor-blog-langcode` and `notxor-blog-texts`."
  (let* ((text-node (assoc text-id notxor-blog-texts))
	 (text-lang-node (if text-node
			     (assoc notxor-blog-langcode text-node)
			   nil)))
    (if text-lang-node
	(cdr text-lang-node)
      (concat "[" (symbol-name text-id) ":" notxor-blog-langcode "]"))))


;;;###autoload
(defun notxor-blog-publicar ()
  "Renderiza todos los artículos, el index, el archive, tags y los RSS.
Sólo lo hace con los archivos nuevos y los modificados después de haberse
publicado el HTML."
  (interactive "P")
  (dolist (file (append (notxor-blog-get-post-filenames)))
    (when (notxor-blog-needs-publishing-p file)
      (notxor-blog-publish-file file)))
  (notxor-blog-assemble-index)
  (notxor-blog-assemble-rss)
  (notxor-blog-assemble-archive)
  (if notxor-blog-enable-rss-by-tag
      (notxor-blog-assemble-rss-by-tag))
  (if notxor-blog-enable-tags
      (notxor-blog-assemble-tags)))

(defun notxor-blog-needs-publishing-p (post-filename)
  "Check whether POST-FILENAME was changed since last render."
  (let ((pub-filename
         (notxor-blog-matching-publish-filename post-filename)))
    (not (and (file-exists-p pub-filename)
              (file-newer-than-file-p pub-filename post-filename)))))

(defun notxor-blog-matching-publish-filename (post-filename)
  "Generate HTML file name for POST-FILENAME."
  (concat notxor-blog-publish-directory
          (notxor-blog-get-post-public-path post-filename)))

(defun notxor-blog-get-post-filenames ()
  "Return a list of all posts."
  (directory-files-recursively
   notxor-blog-posts-directory ".*\\.org$"))

(defun notxor-blog-get-draft-filenames ()
  "Return a list of all drafts."
  (directory-files-recursively
   notxor-blog-drafts-directory ".*\\.org$"))

(defun notxor-blog-file-buffer (file)
  "Return the buffer open with a full filepath of FILE, or nil."
  (require 'seq)
  (make-directory (file-name-directory file) t)
  (car (seq-filter
         (lambda (buf)
           (string= (with-current-buffer buf buffer-file-name) file))
         (buffer-list))))

;; This macro is needed for many of the following functions.
(defmacro notxor-blog-with-find-file (file contents &rest body)
  "Execute BODY in FILE.  Use this to insert text of CONTENTS into FILE.
The buffer is disposed after the macro exits (unless it already
existed before)."
  `(save-excursion
     (let ((current-buffer (current-buffer))
           (buffer-exists (notxor-blog-file-buffer ,file))
           (result nil)
	   (contents ,contents))
       (if buffer-exists
           (switch-to-buffer buffer-exists)
         (find-file ,file))
       (erase-buffer)
       (insert contents)
       (setq result (progn ,@body))
       (basic-save-buffer)
       (unless buffer-exists
         (kill-buffer))
       (switch-to-buffer current-buffer)
       result)))

(defun notxor-blog-get-date (post-filename)
  "Extract the `#+date:` from POST-FILENAME as date-time."
  (let ((case-fold-search t))
    (with-temp-buffer
      (insert-file-contents post-filename)
      (goto-char (point-min))
      (if (search-forward-regexp "^\\#\\+date:[ ]*<\\([^]>]+\\)>$" nil t)
	  (date-to-time (match-string 1))
	(time-since 0)))))

(defun notxor-blog-get-title (post-filename)
  "Extract the `#+title:` from POST-FILENAME."
  (let ((case-fold-search t))
    (with-temp-buffer
      (insert-file-contents post-filename)
      (goto-char (point-min))
      (search-forward-regexp "^\\#\\+title:[ ]*\\(.+\\)$")
      (match-string 1))))

(defun notxor-blog-get-author (post-filename)
  "Extract the `#+author:` from POST-FILENAME."
  (let ((case-fold-search t))
    (with-temp-buffer
      (insert-file-contents post-filename)
      (goto-char (point-min))
      (search-forward-regexp "^\\#\\+author:[ ]*\\(.+\\)$")
      (match-string 1))))

(defun notxor-blog-get-tags (post-filename)
  "Extract the `#+filetags:` from POST-FILENAME as list of strings."
  (let ((case-fold-search t))
    (with-temp-buffer
      (insert-file-contents post-filename)
      (goto-char (point-min))
      (if (search-forward-regexp "^\\#\\+filetags:[ ]*:\\(.*\\):$" nil t)
          (split-string (match-string 1) ":")
	(if (search-forward-regexp "^\\#\\+filetags:[ ]*\\(.+\\)$" nil t)
            (split-string (match-string 1))
	  )))))

(defun notxor-blog-get-tag-tree ()
  "Return an association list of tags to filenames.
e.g. `((`foo' `ile1.org' `file2.org') (`bar' `file2.org'))`"
  (let ((tag-tree '()))
    (dolist (post-filename (notxor-blog-get-post-filenames))
      (let ((tags (notxor-blog-get-tags post-filename)))
        (dolist (tag tags)
          (if (assoc-string tag tag-tree t)
              (push post-filename (cdr (assoc-string tag tag-tree t)))
            (push (cons tag (list post-filename)) tag-tree)))))
    tag-tree))

(defun notxor-blog-get-preview (post-filename)
  "Get data from POST-FILENAME for render preview HTML.
If the HTML body contains multiple paragraphs, include only the
first paragraph, and display an ellipsis.  Preamble and Postamble
are excluded, too."
  (with-temp-buffer
    (insert-file-contents (notxor-blog-matching-publish-filename post-filename))
    (let ((post-title)
          (post-date)
          (post-author)
          (post-taglist)
          (post-ellipsis "")
          (first-paragraph-start)
          (first-paragraph-end))
      (setq post-title (notxor-blog-get-title post-filename))
      (setq post-date (notxor-blog-get-date post-filename))
      (setq post-author (notxor-blog-get-author post-filename))
      (setq post-taglist (notxor-blog-post-taglist post-filename))
      ;; Find where the first paragraph ends and starts
      (goto-char (point-min))
      (when (search-forward "<p>" nil t)
        (search-forward "</p>")
        (setq first-paragraph-end (point))
        (search-backward "<p>")
        (setq first-paragraph-start (point))
        (goto-char first-paragraph-end)
        (when (search-forward "<p>" nil t)
          (setq post-ellipsis (concat (when notxor-blog-preview-link-p
                                        (format "<a href=\"%s\">"
                                                (concat "/" (format-time-string "%Y/%m/%d" post-date)
                                                (notxor-blog-get-local-url post-filename))))
                                      notxor-blog-preview-ellipsis
                                      (when notxor-blog-preview-link-p "</a>\n")))))
      ;; Put the substrings together.
      (concat
       (format "<h2 class=\"post-title\"><a href=\"%s\">%s</a></h2>" (concat "/" (format-time-string "%Y/%m/%d" post-date) (notxor-blog-get-local-url post-filename)) post-title)
       (format "<div class=\"post-author\">%s</div>" post-author)
       (format-time-string (concat "<div class=\"post-date\">" (notxor-blog-gettext 'date-format) "</div>") post-date)
       (buffer-substring-no-properties first-paragraph-start first-paragraph-end)
       post-ellipsis
       (format "<div class=\"taglist\">%s</div>" post-taglist)))))


(defun notxor-blog-get-body (post-filename &optional exclude-title)
  "Get the rendered HTML body without headers from POST-FILENAME.
If EXCLUDE-TITLE is prsent, title are excluded.  Preamble and
Postamble are excluded, too."
  (with-temp-buffer
    (insert-file-contents (notxor-blog-matching-publish-filename post-filename))
    (buffer-substring-no-properties
     (progn
       (goto-char (point-min))
       (if exclude-title
           (progn (search-forward "<h1 class=\"post-title\">")
                  (search-forward "</h1>"))
         (search-forward "<div id=\"content\">"))
       (point))
     (progn
       (goto-char (point-max))
       (search-backward "<div id=\"postamble\" class=\"status\">")
       (search-backward "<div id=\"comments\">" nil t)
       (search-backward "</div>")
       (point)))))

(defun notxor-blog-get-absolute-url (relative-url)
  "Return absolute URL based on the RELATIVE-URL passed to the function.

For example, when `notxor-blog-publish-url' is set to `https://example.com/'
and `relative-url' is passed as `archive.html' then the function
will return `https://example.com/archive.html'."
  (concat notxor-blog-publish-url relative-url))

(defun notxor-blog-get-local-url (relative-url)
  "Esta es mía...Return local URL based on the RELATIVE-URL passed to the function."
  (concat "/" (notxor-blog-get-relative-path relative-url)))

(defun notxor-blog-get-post-url (post-filename)
  "Return absolute URL to the published POST-FILENAME.

This function concatenates publish URL and generated custom filepath to the
published HTML version of the post."
  (notxor-blog-get-absolute-url
          (notxor-blog-get-post-public-path post-filename)))

(defun notxor-blog-get-post-public-path (post-filename)
  "Return post filepath in public directory.

This function retrieves relative path to the post file in posts
or drafts directories, the date of the post from its contents and
then passes it to `notxor-blog-generate-post-path` to
generate custom filepath for the published HTML version of the
POST-FILENAME."
  (notxor-blog-generate-post-path
   (notxor-blog-get-relative-path post-filename)
   (notxor-blog-get-date post-filename)))

(defun notxor-blog-get-relative-path (post-filename)
  "Remove absolute directory path from POST-FILENAME.
Change file extention from `.org' to `.html'.  Returns filepath
to HTML file relative to posts or drafts directories.

Works with both posts and drafts directories.

For example, when `notxor-blog-posts-directory' is set to
`~/blog/posts' and `post-filename' is passed as
`~/blog/posts/my-life-update.org' then the function will return
`my-life-update.html'."
  (replace-regexp-in-string ".org$" ".html"
                            (replace-regexp-in-string
                             (concat "^\\("
                                     (file-truename notxor-blog-posts-directory)
                                     "\\|"
                                     (file-truename notxor-blog-drafts-directory)
                                     "\\)")
                             ""
                             (file-truename post-filename))))

(defun notxor-blog-generate-post-path (post-filename post-datetime)
  "Return post public path based on POST-FILENAME and POST-DATETIME.

By default, this function returns post filepath unmodified, so script will
replicate file and directory structure of posts and drafts directories.

Override this function if you want to generate custom post URLs different
from how they are stored in posts and drafts directories.

For example, there is a post in posts directory with the
file path `hobby/charity-coding.org` and dated '<2019-08-20 Tue>'.

In this case, the function will receive following argument values:
- post-filename: `hobby/charity-coding'
- post-datetime: datetime of <2019-08-20 Tue>

and by default will return `hobby/charity-coding', so that the path
to HTML file in publish directory will be `hobby/charity-coding.html'.

If this function is overriden with something like this:

 (defun notxor-blog-generate-post-path (post-filename post-datetime)
  (concat (format-time-string \"%Y/%m/%d\" post-datetime)
          \"/\"
          (file-name-nondirectory post-filename)))

Then the output will be `2019/08/20/charity-coding' and this will be
the path to the HTML file in publish directory and the url for the post.

Pro defecto es:
  post-filename)"
  (concat (format-time-string "%Y/%m/%d" post-datetime)
          "/"
          (file-name-nondirectory post-filename)))

;;;###autoload
(defun notxor-blog-publish-file (post-filename)
  "Publish a single POST-FILENAME.
The index, archive, tags, and RSS feed are not updated."
  (interactive "f")
  (notxor-blog-with-find-file
   (notxor-blog-matching-publish-filename post-filename)
   (concat
    "<!DOCTYPE html>\n"
    "<html lang=\"" notxor-blog-langcode "\">\n"
    "<head>\n"
    "<meta charset=\"UTF-8\">\n"
    "<link rel=\"alternate\"\n"
    "      type=\"application/rss+xml\"\n"
    "      href=\"" (notxor-blog-get-absolute-url notxor-blog-rss-file) "\"\n"
    "      title=\"RSS feed for " notxor-blog-publish-url "\"/>\n"
    "<title>" (notxor-blog-get-title post-filename) "</title>\n"
    notxor-blog-page-header
    "</head>\n"
    "<body>\n"
    "<div id=\"preamble\" class=\"status\">\n"
    notxor-blog-page-preamble
    "</div>\n"
    "<div id=\"content\">\n"
    (notxor-blog-post-preamble post-filename)
    (notxor-blog-render-post-content post-filename)
    (notxor-blog-post-postamble post-filename)
    "</div>\n"
    "<div id=\"postamble\" class=\"status\">"
    notxor-blog-page-postamble
    "</div>\n"
    "</body>\n"
    "</html>\n")))

(defun notxor-blog-render-post-content (post-filename)
  "Render blog content of POST-FILENAME as bare HTML without header."
  (let ((org-html-doctype "html5")
        (org-html-html5-fancy t))
    (save-excursion
      (let ((current-buffer (current-buffer))
	    (buffer-exists (notxor-blog-file-buffer post-filename))
	    (result nil))
	(if buffer-exists
	    (switch-to-buffer buffer-exists)
	  (find-file post-filename))
	(setq result
	      (org-export-as 'notxor-blog-post-bare nil nil nil nil))
	(basic-save-buffer)
	(unless buffer-exists
	  (kill-buffer))
	(switch-to-buffer current-buffer)
	result))))

(org-export-define-derived-backend 'notxor-blog-post-bare 'html
  :translate-alist '((template . (lambda (contents info) contents))))

(defun notxor-blog-assemble-index ()
  "Assemble the blog index page.
The index page contains the last `notxor-blog-index-length`
posts as full text posts."
  (let ((post-filenames (notxor-blog-get-post-filenames)))
    ;; reverse-sort, so that the later `last` will grab the newest posts
    (setq post-filenames (sort post-filenames (lambda (x y) (time-less-p (notxor-blog-get-date x)
                                                                         (notxor-blog-get-date y)))))
    (notxor-blog-assemble-multipost-page
     (concat notxor-blog-publish-directory notxor-blog-index-file)
     (last post-filenames notxor-blog-index-length))))

(defun notxor-blog-assemble-multipost-page (pub-filename post-filenames &optional front-matter)
  "Assemble a page PUB-FILENAME that contain multiple POST-FILENAMES.
Posts are sorted in descending time.  FRONT-MATTER is opcional."
  (setq post-filenames (sort post-filenames (lambda (x y) (time-less-p (notxor-blog-get-date y)
                                                                  (notxor-blog-get-date x)))))
  (notxor-blog-with-find-file
   pub-filename
   (concat
    "<!DOCTYPE html>\n"
    "<html lang=\"" notxor-blog-langcode "\">\n"
    "<head>\n"
    "<meta charset=\"UTF-8\">\n"
    "<link rel=\"alternate\"\n"
    "      type=\"application/rss+xml\"\n"
    "      href=\"" (notxor-blog-get-absolute-url notxor-blog-rss-file) "\"\n"
    "      title=\"RSS feed for " notxor-blog-publish-url "\"/>\n"
    "<title>" notxor-blog-publish-title "</title>\n"
    notxor-blog-page-header
    "</head>\n"
    "<body>\n"
    "<div id=\"preamble\" class=\"status\">"
    notxor-blog-page-preamble
    "</div>\n"
    "<div id=\"content\">\n"
    (when front-matter front-matter)
    (apply 'concat (mapcar
                    (if notxor-blog-use-preview
                        'notxor-blog-get-preview
                      'notxor-blog-get-body) post-filenames))
    "<div id=\"archive\">\n"
    ;"<a href=\"" (notxor-blog-get-absolute-url notxor-blog-archive-file) "\">" (notxor-blog-gettext 'other-posts) "</a>\n"
    "<a href=\"" (concat "/" notxor-blog-archive-file) "\">" (notxor-blog-gettext 'other-posts) "</a>\n"
    "</div>\n"
    "</div>\n"
    "<div id=\"postamble\" class=\"status\">"
    notxor-blog-page-postamble
    "</div>\n"
    "</body>\n"
    "</html>\n")))

(defun notxor-blog-post-preamble (post-filename)
  "Return the formatted date and headline of the POST-FILENAME.
This function is called for every post and prepended to the post body.
Modify this function if you want to change a posts headline."
  (let ((post-date (notxor-blog-get-date post-filename))
        (post-author (notxor-blog-get-author post-filename)))
    (concat
     "<h1 class=\"post-title\">"
     "<a href=\"" (concat "/" (format-time-string "%Y/%m/%d" post-date) (notxor-blog-get-local-url post-filename)) "\">" (notxor-blog-get-title post-filename) "</a>"
     "</h1>\n"
     "<div class=\"post-author\">" post-author "</div>\n"
     "<div class=\"post-date\">" (format-time-string (notxor-blog-gettext 'date-format)
	                                             (notxor-blog-get-date post-filename))
     "</div>\n")))


(defun notxor-blog-post-taglist (post-filename)
  "Return the tag list of the POST-FILENAME.
This part will be attached at the end of the post, after
the taglist, in a <div id=\"taglist\">...</div> block."
  (let ((taglist-content ""))
    (when (and (notxor-blog-get-tags post-filename) notxor-blog-enable-tags)
      (setq taglist-content (concat "<a href=\""
                                    (concat "/" notxor-blog-tags-file)
                                    "\">" (notxor-blog-gettext 'tags) "</a>: "))
      (dolist (tag (notxor-blog-get-tags post-filename))
        (setq taglist-content (concat taglist-content "<a href=\""
                                      (concat "/tags/" (downcase tag) "/index.html")
                                      "\">" tag "</a> "))))
    taglist-content))


(defun notxor-blog-post-postamble (post-filename)
  "Return the tag list and comment box at the end of a POST-FILENAME.
This function is called for every post and the returned string is
appended to the post body, and includes the tag list generated by
followed by the HTML code for comments."
  (concat "<div class=\"taglist\">"
          (notxor-blog-post-taglist post-filename)
          "</div>"
          (if (string= notxor-blog-post-comments "")
              ""
            (concat "\n<div id=\"comments\">"
                    notxor-blog-post-comments
                    "</div>"))))

(defun notxor-blog-assemble-rss ()
  "Assemble the blog RSS feed.
The RSS-feed is an XML file that contains every blog post in a
machine-readable format."
  (let ((system-time-locale "en_US.utf-8") ; force dates to render as per RSS spec
        (rss-filename (concat notxor-blog-publish-directory notxor-blog-rss-file))
        (rss-items nil))
    (dolist (post-filename (notxor-blog-get-post-filenames))
      (let ((rss-date (notxor-blog-get-date post-filename))
            (rss-text (notxor-blog-get-rss-item post-filename)))
        (add-to-list 'rss-items (cons rss-date rss-text))))
    (setq rss-items (sort rss-items (lambda (x y) (time-less-p (car y) (car x)))))
    (notxor-blog-with-find-file
     rss-filename
     (concat "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
	     "<rss version=\"2.0\">\n"
	     "<channel>\n"
	     "<title><![CDATA[" notxor-blog-publish-title "]]></title>\n"
	     "<description><![CDATA[" notxor-blog-publish-title "]]></description>\n"
	     "<link>" notxor-blog-publish-url "</link>\n"
	     "<lastBuildDate>" (format-time-string "%a, %d %b %Y %H:%M:%S %z" (current-time)) "</lastBuildDate>\n"
             notxor-blog-rss-extra
	     (apply 'concat (mapcar 'cdr rss-items))
	     "</channel>\n"
	     "</rss>\n"))))

(defun notxor-blog-get-rss-item (post-filename)
  "Assemble RSS item from POST-FILENAME.
The HTML content is taken from the rendered HTML post."
  (concat
   "<item>\n"
   "  <title><![CDATA[" (notxor-blog-get-title post-filename) "]]></title>\n"
   "  <description><![CDATA["
   (notxor-blog-get-body post-filename t) ; exclude headline!
   "]]></description>\n"
   (let ((categories ""))
     (when (and (notxor-blog-get-tags post-filename) notxor-blog-enable-tags)
       (dolist (tag (notxor-blog-get-tags post-filename))
         (setq categories (concat categories
                                  "  <category><![CDATA[" tag "]]></category>\n"))))
     categories)
   "  <link>"
   (notxor-blog-get-post-url post-filename)
   "</link>\n"
   "  <pubDate>"
   (let ((system-time-locale "en_US.utf-8")) ; force dates to render as per RSS spec
     (format-time-string "%a, %d %b %Y %H:%M:%S %z" (notxor-blog-get-date post-filename)))
   "</pubDate>\n"
   "</item>\n"))

(defun notxor-blog-assemble-rss-by-tag ()
  "Assemble the RSS for tags."
  (interactive)
  (mapcar 'notxor-blog-rss-for-tag (notxor-blog-get-tag-tree)))

(defun notxor-blog-rss-for-tag (tag)
  "Assemble the TAG RSS feed.
The RSS-feed is an XML file that contains every blog post in a
machine-readable format."
  (let ((system-time-locale "en_US.utf-8") ; force dates to render as per RSS spec
        (rss-filename (concat notxor-blog-publish-directory "/tags/" (car tag) "/" notxor-blog-rss-file))
        (rss-items nil))
    (dolist (post-filename (cdr tag))
      (let ((rss-date (notxor-blog-get-date post-filename))
            (rss-text (notxor-blog-get-rss-item post-filename)))
        (add-to-list 'rss-items (cons rss-date rss-text)))
    (setq rss-items (sort rss-items (lambda (x y) (time-less-p (car y) (car x)))))
    (notxor-blog-with-find-file
     rss-filename
     (concat "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
	     "<rss version=\"2.0\">\n"
	     "<channel>\n"
	     "<title><![CDATA[" notxor-blog-publish-title "]]></title>\n"
	     "<description><![CDATA[" notxor-blog-publish-title "]]></description>\n"
	     "<link>" notxor-blog-publish-url "</link>\n"
	     "<lastBuildDate>" (format-time-string "%a, %d %b %Y %H:%M:%S %z" (current-time)) "</lastBuildDate>\n"
             notxor-blog-rss-extra
	     (apply 'concat (mapcar 'cdr rss-items))
	     "</channel>\n"
	     "</rss>\n")))))

(defun notxor-blog-assemble-archive ()
  "Re-render the blog archive page.
The archive page contains single-line links and dates for every
blog post, but no post body."
  (let ((archive-filename (concat notxor-blog-publish-directory notxor-blog-archive-file))
        (archive-entries nil)
        (post-filenames (notxor-blog-get-post-filenames)))
    (setq post-filenames (sort post-filenames (lambda (x y) (time-less-p
                                                        (notxor-blog-get-date y)
                                                        (notxor-blog-get-date x)))))
    (notxor-blog-with-find-file
     archive-filename
     (concat
      "<!DOCTYPE html>\n"
      "<html lang=\"" notxor-blog-langcode "\">\n"
      "<head>\n"
      "<meta charset=\"UTF-8\">\n"
      "<link rel=\"alternate\"\n"
      "      type=\"application/rss+xml\"\n"
      "      href=\"" (notxor-blog-get-absolute-url notxor-blog-rss-file) "\"\n"
      "      title=\"RSS feed for " notxor-blog-publish-url "\">\n"
      "<title>" notxor-blog-publish-title "</title>\n"
      notxor-blog-page-header
      "</head>\n"
      "<body>\n"
      "<div id=\"preamble\" class=\"status\">\n"
      notxor-blog-page-preamble
      "</div>\n"
      "<div id=\"content\">\n"
      "<h1 class=\"title\">" (notxor-blog-gettext 'archive) "</h1>\n"
      (apply 'concat (mapcar 'notxor-blog-get-post-summary post-filenames))
      "</div>\n"
      "<div id=\"postamble\" class=\"status\">"
      notxor-blog-page-postamble
      "</div>\n"
      "</body>\n"
      "</html>"))))

(defun notxor-blog-get-post-summary (post-filename)
  "Assemble POST-FILENAME summary for an archive page.
This function is called for every post on the archive and
tags-archive page.  Modify this function if you want to change an
archive headline."
  (let ((post-date (notxor-blog-get-date post-filename)))
    (concat
     "<h2 class=\"post-title-summary\">"
     "<a href=\"" (concat "/" (format-time-string "%Y/%m/%d" post-date) (notxor-blog-get-local-url post-filename)) "\">" (notxor-blog-get-title post-filename) "</a>"
     "</h2>"
     "<div class=\"post-date-summary\">"
     (format-time-string (notxor-blog-gettext 'date-format) post-date)
     "</div>\n")))

(defun notxor-blog-assemble-tags ()
  "Render the tag archive and tag pages."
  (notxor-blog-assemble-tags-archive)
  (dolist (tag (notxor-blog-get-tag-tree))
    (notxor-blog-assemble-multipost-page
     (concat notxor-blog-publish-directory "/tags/" (downcase (car tag)) "/index.html")
     (cdr tag)
     (concat "<h1 class=\"title\">" (notxor-blog-gettext 'posts-tagged) " \"" (car tag) "\":</h1>"))))

(defun notxor-blog-assemble-tags-archive-tag (tag)
  "Assemble single TAG for all filenames."
  (let ((post-filenames (cdr tag)))
    (setq post-filenames
	  (sort post-filenames (lambda (x y) (time-less-p (notxor-blog-get-date x)
						     (notxor-blog-get-date y)))))
    (concat "<h1 class=\"tags-title\">" (notxor-blog-gettext 'posts-tagged) " «" (downcase (car tag)) "»:</h1>\n"
	    (apply 'concat (mapcar 'notxor-blog-get-post-summary post-filenames)))))

(defun notxor-blog-assemble-tags-archive ()
  "Assemble the blog tag archive page.
The archive page contains single-line links and dates for every
blog post, sorted by tags, but no post body."
  (let ((tags-archive-filename (concat notxor-blog-publish-directory notxor-blog-tags-file))
        (tag-tree (notxor-blog-get-tag-tree)))
    (setq tag-tree (sort tag-tree (lambda (x y) (string-greaterp (car y) (car x)))))
    (notxor-blog-with-find-file
     tags-archive-filename
     (concat
      "<!DOCTYPE html>\n"
      "<html lang=\"" notxor-blog-langcode "\">\n"
      "<head>\n"
      "<meta charset=\"UTF-8\">\n"
      "<link rel=\"alternate\"\n"
      "      type=\"application/rss+xml\"\n"
      "      href=\"" (notxor-blog-get-absolute-url notxor-blog-rss-file) "\"\n"
      "      title=\"RSS feed for " notxor-blog-publish-url "\">\n"
      "<title>" notxor-blog-publish-title "</title>\n"
      notxor-blog-page-header
      "</head>\n"
      "<body>\n"
      "<div id=\"preamble\" class=\"status\">"
      notxor-blog-page-preamble
      "</div>\n"
      "<div id=\"content\">\n"
      "<h1 class=\"title\">" (notxor-blog-gettext 'tags) "</h1>\n"
      (apply 'concat (mapcar 'notxor-blog-assemble-tags-archive-tag tag-tree))
      "</div>\n"
      "<div id=\"postamble\" class=\"status\">"
      notxor-blog-page-postamble
      "</div>\n"
      "</body>\n"
      "</html>\n"))))

(defun notxor-blog-open-previous-post ()
  "Opens previous blog post."
  (interactive)
  (let ((posts (sort (notxor-blog-get-post-filenames)
                     (lambda (x y)
                       (time-less-p (notxor-blog-get-date y)
                                    (notxor-blog-get-date x)))))
        (current-post (buffer-file-name)))
    (while (and posts
                (not (string-equal
                      (file-name-nondirectory current-post)
                      (file-name-nondirectory (car posts)))))
      (setq posts (cdr posts)))
    (if (> (cl-list-length posts) 1)
        (find-file (cadr posts))
      (message (notxor-blog-gettext 'no-prev-post)))))

(defun notxor-blog-open-next-post ()
  "Opens next blog post."
  (interactive)
  (let ((posts (sort (notxor-blog-get-post-filenames)
                     (lambda (x y)
                       (time-less-p (notxor-blog-get-date x)
                                    (notxor-blog-get-date y)))))
        (current-post (buffer-file-name)))
    (while (and posts
                (not (string-equal
                      (file-name-nondirectory current-post)
                      (file-name-nondirectory (car posts)))))
      (setq posts (cdr posts)))
    (if (> (cl-list-length posts) 1)
        (find-file (cadr posts))
      (message (notxor-blog-gettext 'no-next-post)))))

(defun notxor-blog-open-matching-publish-file ()
  "Opens HTML for post."
  (interactive)
  (find-file (notxor-blog-matching-publish-filename (buffer-file-name))))

;;;###autoload
(defun notxor-blog-create-new-post (&optional draft)
  "Create a new blog post.
Prompts for a title and proposes a file name.  The file name is
only a suggestion; You can choose any other file name if you so
choose.  if DRAFT, create a new blog draft."
  (interactive)
  (let ((title (read-string (notxor-blog-gettext 'title))))
    (find-file (concat
                (if draft
                    notxor-blog-drafts-directory
                    notxor-blog-posts-directory)
                (read-string (notxor-blog-gettext 'filename)
			     (concat (format-time-string "%Y-%m-%d-" (current-time))
				     (replace-regexp-in-string "\s" "-" (downcase title))
				     ".org"))))
    (insert "#+title:    " title "\n"
            "#+date:     " (format-time-string "<%Y-%m-%d %H:%M>") "\n"
            "#+author:   " "\n"
            "#+filetags: " "\n"
            "#+language: es\n"
            "#+macro: nota @@html:<span id=\"nota\">@@$1@@html:<span class=\"nota\">@@$2@@html:</span></span>@@" "\n"
            "#+options:  H:4 num:nil toc:nil \\n:nil ::t |:t ^:{} -:nil f:t *:t <:t")))

;;;###autoload
(defun notxor-blog-create-new-draft ()
  "Create a new blog draft.
Prompts for a title and proposes a file name.  The file name is
only a suggestion; You can choose any other file name if you so
choose."
  (interactive)
  (notxor-blog-create-new-post 't))


;;;###autoload
(define-derived-mode notxor-blog-mode org-mode "NB"
  "Blogging with `org-mode' and Emacs."
  (run-mode-hooks)
  :group 'notxor-blog)

;; Key bindings
(define-key notxor-blog-mode-map (kbd "C-c C-f") 'notxor-blog-open-next-post)
(define-key notxor-blog-mode-map (kbd "C-c C-b") 'notxor-blog-open-previous-post)
(define-key notxor-blog-mode-map (kbd "C-c C-p") 'notxor-blog-open-matching-publish-file)
(define-key notxor-blog-mode-map (kbd "C-c C-n") 'notxor-blog-create-new-post)
(define-key notxor-blog-mode-map (kbd "C-c C-d") 'notxor-blog-create-new-draft)

(provide 'notxor-blog)

;;; notxor-blog.el ends here
